#!/usr/bin/env sh

echo "Starting powerdns $ROLE with $BACKEND backend"

get_supermaster_from_dns() {
  echo "settings supermaster addresses to:"
  host -tA "$1" | awk '{print $4}'
  cat <<EOF > "$BIND_CONFIG_DIR"/supermasters.conf
$(host -tA "$1" | awk '{print $4}')
EOF
}

setup_bind() {
  mkdir -p "$BIND_CONFIG_DIR"/zones
  touch "$BIND_CONFIG_DIR"/named-superslave.conf
  if [ ! -f "$BIND_CONFIG_DIR"/named.conf ]; then
    cat <<EOF > "$BIND_CONFIG_DIR"/named.conf
options {
    directory "/var/lib/pdns";
};

include "named-superslave.conf";
EOF
  fi
  if [ -n "$SUPERMASTER" ] || [ -n "$SUPERMASTER_DOMAIN" ]; then
    if [ -n "$SUPERMASTER_DOMAIN" ]; then
      get_supermaster_from_dns "$SUPERMASTER_DOMAIN"
    else
      for master in $SUPERMASTER; do
        echo "$master" > "$BIND_CONFIG_DIR"/supermasters.conf
      done
    fi

    cat <<EOF >> /etc/pdns/pdns.conf
bind-supermaster-config=$BIND_CONFIG_DIR/named-superslave.conf
bind-supermasters=$BIND_CONFIG_DIR/supermasters.conf
bind-supermaster-destdir=$BIND_CONFIG_DIR/zones
superslave=yes
EOF
  fi
}

case "$BACKEND" in
  "psql")
    cat <<EOF >> /etc/pdns/pdns.conf
launch=gpgsql
gpgsql-host=$DB_HOST
gpgsql-port=$DB_PORT
gpgsql-dbname=$DB_NAME
gpgsql-user=$DB_USER
gpgsql-password=$DB_PASS
EOF
    ;;
  "mysql")
    cat <<EOF >> /etc/pdns/pdns.conf
launch=gmysql
gmysql-host=$DB_HOST
gpgsql-port=$DB_PORT
gpgsql-dbname=$DB_NAME
gpgsql-user=$DB_USER
gpgsql-password=$DB_PASS
EOF
    ;;
  "bind")
    cat <<EOF >> /etc/pdns/pdns.conf
launch=bind
bind-config=$BIND_CONFIG_DIR/named.conf
EOF
    setup_bind
    ;;
esac

case "$ROLE" in
  "master")
    ROLE_OPTIONS="--master"
    ;;
  "slave")
    if [ -n "$SUPERSLAVE" ]; then
      ROLE_OPTIONS="--slave --superslave"
    else
      ROLE_OPTIONS="--slave"
    fi
    ;;
esac

# Run pdns server
trap "pdns_control quit" SIGHUP SIGINT SIGTERM

mkdir -p /var/run/pdns /etc/pdns/conf.d
chown pdns:pdns -R /var/run/pdns /var/lib/pdns

pdns_server --daemon=no $BACKEND_OPTIONS $ROLE_OPTIONS "$@"
